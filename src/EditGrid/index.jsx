import React, { useState } from 'react';
import './index.css';
import Editing from '../Editing';
import ImgList from '../Editing/ImgList';

const EditGrid = () => {
    const [images, setImages] = useState([]);

    return (
        <div className="flex-container">
        <div style={{height: "300px"}}>
            <ImgList images={images} setImages={setImages} />
        </div>
            

            <div style={{ width: "70%" }}>
                <Editing images={images} />
            </div>
        </div>
    )
}

export default EditGrid;