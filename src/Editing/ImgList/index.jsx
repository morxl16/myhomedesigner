import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import lionImg from '../../Assets/lion.png';
import sofa1 from '../../Assets/sofa1.png';
import sofa3 from '../../Assets/sofa3.png';
import sofa4 from '../../Assets/sofa4.png';
import chair1 from '../../Assets/chair1.png';
import table2 from '../../Assets/table2.png';
import table3 from '../../Assets/table3.png';


const imgList = [
    {
        img: sofa1,
        title: 'ספה',
        subtitle: 'איטלקית לבנה',
        width: 300,
        height: 200
    },
    {
        img: sofa4,
        title: 'ספה',
        subtitle: 'בד',
        width: 300,
        height: 200
    },
    {
        img: sofa3,
        title: 'ספה',
        subtitle: 'שחור לבן',
        width: 300,
        height: 200
    },
    {
        img: chair1,
        title: 'כורסה',
        subtitle: 'קש',
        width: 200,
        height: 200
    },
    {
        img: table2,
        title: 'שולחן',
        subtitle: 'עץ',
        width: 200,
        height: 200
    },
    {
        img: table3,
        title: 'שולחן',
        subtitle: 'עץ לבן',
        width: 200,
        height: 200
    },
];

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: 500,
        height: 450,
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
});

const TitlebarGridList = (props) => {
    const { classes } = props;

    const addImage = (imgTile) => {

        return () => {
            props.setImages([imgTile, ...props.images]);
        };
    }

    return (
        <div className={classes.root} dir="rtl">
            <GridList cellHeight={180} className={classes.gridList}>
                {/* <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
                    <ListSubheader component="div">December</ListSubheader>
                </GridListTile> */}
                {imgList.map(tile => (
                    <GridListTile key={JSON.stringify(tile)}>
                        <img src={tile.img} alt={tile.title} />
                        <GridListTileBar
                            title={tile.title}
                            subtitle={<span>{tile.subtitle}</span>}
                            actionIcon={
                                <IconButton className={classes.icon} onClick={addImage(tile)}>
                                    <i className="fa fa-plus-circle"></i>
                                </IconButton>
                            }
                        />
                    </GridListTile>
                ))}
            </GridList>
        </div>
    );
}

TitlebarGridList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TitlebarGridList);