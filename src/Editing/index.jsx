import React, { Component } from 'react';
import { Stage, Layer, Image } from 'react-konva';
import Konva from "konva";
import room from '../Assets/room.png';

// the first very simple and recommended way:
const handleDragStart = e => {
    e.target.setAttrs({
        shadowOffset: {
            x: 15,
            y: 15
        },
        scaleX: 1.1,
        scaleY: 1.1
    });
};

const handleDragEnd = e => {
    e.target.to({
        duration: 0.5,
        easing: Konva.Easings.ElasticEaseOut,
        scaleX: 1,
        scaleY: 1,
        shadowOffsetX: 5,
        shadowOffsetY: 5
    });
};

class URLImage extends React.Component {
    state = {
        image: null
    };

    componentDidMount() {
        this.loadImage();
    }
    componentDidUpdate(oldProps) {
        if (oldProps.src !== this.props.src) {
            this.loadImage();
        }
    }
    componentWillUnmount() {
        this.image.removeEventListener('load', this.handleLoad);
    }
    loadImage() {
        // save to "this" to remove "load" handler on unmount
        this.image = new window.Image();
        this.image.src = this.props.src;
        this.image.addEventListener('load', this.handleLoad);
    }
    handleLoad = () => {
        // after setState react-konva will update canvas and redraw the layer
        // because "image" property is changed
        this.setState({
            image: this.image
        });
        // if you keep same image object during source updates
        // you will have to update layer manually:
        // this.imageNode.getLayer().batchDraw();
    };
    render() {
        return (
            <Image
                draggable={this.props.draggable}
                onDragStart={handleDragStart}
                onDragEnd={handleDragEnd}
                x={this.props.x}
                y={this.props.y}
                image={this.state.image}
                ref={node => {
                    this.imageNode = node;
                }}
                width={this.props.width}
                height={this.props.height}
            />
        );
    }
}

class Editing extends Component {

    render() {
        return (
            <div style={{ backgroundImage: room }}>
                <Stage backgroundImage={room} width={window.innerWidth} height={window.innerHeight}>
                    <Layer>
                        <URLImage src={room} draggable={false}/>
                        {
                            this.props.images.map(
                                (img, idx) => {
                                    return <URLImage key={idx}
                                        src={img.img}
                                        // x={50 * idx}
                                        width={img.width}
                                        height={img.height}
                                        draggable={true}
                                    />
                                }
                            )
                        }

                    </Layer>
                </Stage>
            </div>
        );
    }
}

export default Editing;
