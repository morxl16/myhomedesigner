import React from 'react';
import './App.css';
import EditGrid from './EditGrid';

import '../node_modules/font-awesome/css/font-awesome.css';

function App() {
    return (
        <div>
            <EditGrid />
        </div>
    );
}

export default App;
